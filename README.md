![](tinder.ico)
Like dislike swipe deck tinder style. I followed youtube tutorial from [here](https://www.youtube.com/watch?v=MDAdY2LkP_U) and [here](https://www.youtube.com/watch?v=mmHywzVkKn8) with the exception that I did not use Expo and I used typescript. This is what the end result looks like:
![](swipedeck.gif)