import React from 'react';
import { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    Animated,
    ImageStyle,
    PanResponder,
    PanResponderInstance
} from 'react-native';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

const Users = [
    { id: '1', uri: require('../assets/1.gif') },
    { id: '2', uri: require('../assets/2.gif') },
    { id: '3', uri: require('../assets/3.gif') },
    { id: '4', uri: require('../assets/4.gif') },
    { id: '5', uri: require('../assets/5.gif') },
    { id: '6', uri: require('../assets/6.gif') },
    { id: '7', uri: require('../assets/7.gif') },
    { id: '8', uri: require('../assets/8.gif') },
    { id: '9', uri: require('../assets/9.gif') },
    { id: '10', uri: require('../assets/10.gif') },
    { id: '11', uri: require('../assets/11.gif') },
    { id: '12', uri: require('../assets/12.gif') }
];

export interface Props { }
export interface State {
    currentIndex: number;
}

export default class App extends Component<Props, State> {
    PanResponder: PanResponderInstance;
    position: Animated.ValueXY;
    rotate: Animated.AnimatedInterpolation;
    rotateAndTranslate: Object;
    likeOpacity: Animated.AnimatedInterpolation;
    dislikeOpacity: Animated.AnimatedInterpolation;
    nextCardOpacity: Animated.AnimatedInterpolation;
    nextCardScale: Animated.AnimatedInterpolation;

    constructor(props: Props) {
        super(props);
        this.PanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onPanResponderMove: (evt, gestureState) => {
                this.position.setValue({ x: gestureState.dx, y: gestureState.dy });
            },
            onPanResponderRelease: (evt, gestureState) => {
                if (gestureState.dx > 120) {
                    Animated.spring(this.position, { toValue: { x: SCREEN_WIDTH + 100, y: gestureState.dy}}).start(() => {
                        this.setState({currentIndex: this.state.currentIndex + 1}, () => { this.position.setValue({ x: 0, y: 0}); });
                    });
                } else if (gestureState.dx < -120) {
                    Animated.spring(this.position, { toValue: { x: -SCREEN_WIDTH - 100, y: gestureState.dy}}).start(() => {
                        this.setState({currentIndex: this.state.currentIndex + 1}, () => { this.position.setValue({ x: 0, y: 0}); });
                    });
                } else {
                    Animated.spring(this.position, {
                        toValue: { x: 0, y: 0 },
                        friction: 4
                    }).start();
                }
            }
        });
        this.position = new Animated.ValueXY();
        this.state = {
            currentIndex: 0
        };
        this.rotate = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: ['-10deg', '0deg', '10deg' ],
            extrapolate: 'clamp'
        });
        this.rotateAndTranslate = {
            transform: [{
                rotate: this.rotate
            },
            ...this.position.getTranslateTransform()
            ]
        };
        this.likeOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp'
        });
        this.dislikeOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0, 0],
            extrapolate: 'clamp'
        });
        this.nextCardOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0, 1],
            extrapolate: 'clamp'
        });
        this.nextCardScale = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0.8, 1],
            extrapolate: 'clamp'
        });
    }

    componentWillMount() {
    }

    renderUsers = () => {
        return Users.map((item, i) => {
            if (i < this.state.currentIndex) {
                return null;
            } else if (i === this.state.currentIndex) {
                return(
                    <Animated.View
                        {...this.PanResponder.panHandlers}
                        key={item.id}
                        style={[this.rotateAndTranslate, { height: SCREEN_HEIGHT - 120, width: SCREEN_WIDTH, padding: 10, position: 'absolute'}]}>
                    <Animated.View style={{ opacity: this.likeOpacity, transform: [{rotate: '-30deg'}], position: 'absolute', top: 50, left: 40, zIndex: 1000}}>
                        <Text style={{ borderWidth: 1, borderColor: 'green', color: 'green', fontSize: 32, fontWeight: '800', padding: 10 }}>LIKE</Text>
                    </Animated.View>
                    <Animated.View style={{ opacity: this.dislikeOpacity, transform: [{rotate: '30deg'}], position: 'absolute', top: 50, right: 40, zIndex: 1000}}>
                        <Text style={{ borderWidth: 1, borderColor: 'red', color: 'red', fontSize: 32, fontWeight: '800', padding: 10 }}>NOPE</Text>
                    </Animated.View>
                    <Image
                        style={{flex: 1, height: undefined, width: undefined, resizeMode: 'cover', borderRadius: 20} as ImageStyle}
                        source={item.uri} />
                    </Animated.View>
                );
            }
            return(
                <Animated.View
                        key={item.id}
                        style={[{ opacity: this.nextCardOpacity, transform: [{scale: this.nextCardScale}], height: SCREEN_HEIGHT - 120, width: SCREEN_WIDTH, padding: 10, position: 'absolute'}]}>
                    <Animated.View style={{ opacity: 0, transform: [{rotate: '-30deg'}], position: 'absolute', top: 50, left: 40, zIndex: 1000}}>
                        <Text style={{ borderWidth: 1, borderColor: 'green', color: 'green', fontSize: 32, fontWeight: '800', padding: 10 }}>LIKE</Text>
                    </Animated.View>
                    <Animated.View style={{ opacity: 0, transform: [{rotate: '30deg'}], position: 'absolute', top: 50, right: 40, zIndex: 1000}}>
                        <Text style={{ borderWidth: 1, borderColor: 'red', color: 'red', fontSize: 32, fontWeight: '800', padding: 10 }}>NOPE</Text>
                    </Animated.View>
                    <Image
                        style={{flex: 1, height: undefined, width: undefined, resizeMode: 'cover', borderRadius: 20} as ImageStyle}
                        source={item.uri} />
                    </Animated.View>
            );
        }).reverse();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 60 }}>
                </View>
                <View style={{ flex: 1 }}>
                    {this.renderUsers()}
                </View>
                <View style={{ height: 60 }}>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
